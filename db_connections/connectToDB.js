const mongoose = require('mongoose');

// this was implemented to switch from cloud to local if there is no internet.
async function connectToDb() {
    const dbURI = process.env.DB_URI_CLOUD;
    mongoose.set('strictQuery', true); // For deprecation warnings
    try {
      await mongoose.connect(dbURI);
      return true;
    } catch (err) {
      return false;
    }
  }
module.exports = {
    connectToDb
}