const { exec, spawn } = require('child_process');
const { deleteAllDocuments } = require('./clearLocalDB')


function animateLoading() {
  const frames = ['-', '\\', '|', '/']; // Define the frames to display
  let frameIndex = 0;

  const animationInterval = setInterval(() => {
    process.stdout.write(`\rProcessing... ${frames[frameIndex]}`);
    frameIndex = (frameIndex + 1) % frames.length;
  }, 100); // Change the interval duration to adjust the speed

  return animationInterval;
}

function syncToAtlas() {
    const command = 'mongoexport --db=comtechdb_local --collection=joborderdbs --out=localdb_data_staging.json';
  
    let loadingAnimation = animateLoading();
    console.log('Exporting data from localdb to json')
    exec(command, (error, stdout, stderr) => {
      clearInterval(loadingAnimation); // Stop the loading animation
  
      if (error) {
        const errorMessage = error ? error.message : stderr;
        console.error(`\n\nAyay!! ERROR EXPORTING DATA TO JSON! Naay error bay :-(\n${errorMessage}`);
        // Perform any necessary actions or error handling here
  
        // Retry the command after a delay
        console.log("Retrying...")
        setTimeout(syncToAtlas, 3000); // Retry after 3 seconds
      } else {
        console.log(`\n${stdout || stderr}Data exported to json successfully!\n`);
        console.log('Importing to Atlas...')
        importToAtlas()
      }
    });
  }
  

function importToAtlas() {
//   const command = 'mongoimport --uri="mongodb+srv://jerryclarkc:rIqFl7FLtNO0JUHm@mylearningprojects.1vfhtdz.mongodb.net/comtechdb?retryWrites=true&w=majority" --ssl --collection=joborderdbs --type=json --file=./localdb_data_staging.json';
  const command = 'mongoimport mongodb+srv://jerryclarkc:rIqFl7FLtNO0JUHm@mylearningprojects.1vfhtdz.mongodb.net/comtechdb --collection=joborderdbs --type=json --file=./localdb_data_staging.json';

  let loadingAnimation = animateLoading();

  exec(command, (error, stdout, stderr) => {
    clearInterval(loadingAnimation); // Stop the loading animation

    if (error) {
      const errorMessage = error ? error.message : stderr;
      console.error(`\n\nAyay!! ERROR IMPORTING DATA TO ATLAS! Naay error bay :-(\n${errorMessage}`);
      // Perform any necessary actions or error handling here

      // Retry the command after a delay
      console.log("Retrying...")
      setTimeout(importToAtlas, 3000); // Retry after 3 seconds
    } else {
      console.log(' \n\nDatabase synced successfully! :-D');
      console.log(stdout || stderr);
      
    //   console.log(' \nClearing the local db..');
    //   deleteAllDocuments(url, dbName, collectionName).then(result => console.log('\nDone! You are now back online. :-D'));
      
    }
  });
}


// function forTesting() { //test
//     const command = 'mongoexport --db=comtechdb_local --collection=joborderdbs --out=localdb_data_staging.json';
  
//     exec(command, (error, stdout, stderr) => {
//         console.log(`Stdout: \n${stdout}`)
//         console.log(`Error: \n${error}`)
//         console.log(`Stderr: \n${stderr}`)

//     });
//   }

// syncToAtlas();

module.exports = {
    syncToAtlas,
    animateLoading,

}