const { MongoClient } = require('mongodb');


const url = 'mongodb://localhost:27017';
const dbName = 'comtechdb_local';
const collectionName = 'joborderdbs';


async function deleteAllDocuments() {
  const client = new MongoClient(url);

  try {
    await client.connect();
    console.log('\n==============================')
    console.log('Connected Local MongoDB server');

    const db = client.db(dbName);
    const collection = db.collection(collectionName);

    const result = await collection.deleteMany({});
    console.log(`Deleted ${result.deletedCount} documents from the collection "${collectionName}"`);
    console.log('Local database is cleared successfully.')
    console.log('==============================\n')
  } catch (error) {
    console.error('Error deleting documents:', error);
    // Perform any necessary error handling here

  } finally {
    client.close();
  }
}

module.exports = {
    deleteAllDocuments
};