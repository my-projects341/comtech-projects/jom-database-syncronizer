
const express = require('express');
//create app
const app = express();

app.use(express.static('public'))
//##############################################
//test
const server = require('http').Server(app);
const io = require('socket.io')(server);
const {deleteAllDocuments} = require('./db_connections/clearLocalDB')
const { exec, spawn } = require('child_process');

// const { animateLoading } = require('./db_connections/syncLocalToCloud')

function syncToAtlas() {
    const command = 'mongoexport';
    const args = ['--db=comtechdb_local', '--collection=joborderdbs', '--out=localdb_data_staging.json'];
    const childProcess = spawn(command, args);
  
    emitConsoleLog('########################################');
    emitConsoleLog('    SYNCRONIZING THE DATABASE...');
    emitConsoleLog('########################################');
    emitConsoleLog('<span style="color: blue;">Linux terminal session started!</span>');
    emitConsoleLog('Exporting data from localdb to json');
    
    childProcess.stdout.on('data', (data) => {
      const consoleOutput = data.toString();
      emitConsoleLog(consoleOutput);
    });
  
    childProcess.stderr.on('data', (data) => {
      const consoleOutput = data.toString();
      emitConsoleLog(consoleOutput);
    });
  
    childProcess.on('error', (error) => {
      emitConsoleLog(`\n\nAyay!! ERROR EXPORTING DATA TO JSON! Naay error bay :-(\n${error.message}`);
      // Perform any necessary actions or error handling here
  
      emitConsoleLog("Retrying...");
      setTimeout(syncToAtlas, 3000); // Retry after 3 seconds
    });
  
    childProcess.on('exit', (code) => {
      if (code === 0) {
        emitConsoleLog('<span style="color: green;">Data exported to json successfully!</span>');
        importToAtlas();
      }
    });
}

function importToAtlas() {
    emitConsoleLog('Importing to Atlas...');

    const command = 'mongoimport mongodb+srv://jerryclarkc:rIqFl7FLtNO0JUHm@mylearningprojects.1vfhtdz.mongodb.net/comtechdb --collection=joborderdbs --type=json --file=./localdb_data_staging.json';

    exec(command, (error, stdout, stderr) => {
    
        if (error) {
          const errorMessage = error ? error.message : stderr;
          emitConsoleLog(`<span style="color: red;">Ayay naay error bay :-( </span>`);
          emitConsoleLog('<span style="color: red;">ERROR IMPORTING DATA TO ATLAS!</span>')
          emitConsoleLog(errorMessage)
          emitConsoleLog('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~')

          // Perform any necessary actions or error handling here
    
          // Retry the command after a delay
          emitConsoleLog('<span style="color: orange;">Retrying...</span>')
          setTimeout(importToAtlas, 2000); // Retry after 3 seconds
        } else {

            emitConsoleLog('==================================');
            emitConsoleLog(stdout || stderr);
            emitConsoleLog('<span style="color: green;">Database synced successfully! :-D</span>');
            emitConsoleLog('done');
            //Delete Documentsss
            deleteAllDocuments()
            
            emitConsoleLog(' Clearing the local db...');
            deleteAllDocuments().then(result => {
                emitConsoleLog('<span style="color: green;">Done! You are now back online. :-D</span>')
                emitConsoleLog('==================================');
                emitConsoleLog(`
                  <h2 style="text-align: center; "><a style="border: 2px solid cyan; border-radius: 5px; color: cyan; text-decoration: none; padding: .5rem;" href="http://192.168.1.205:4000/job-orders">CLICK HERE TO PROCEED TO THE DASHBOARD &gt;&gt;&gt;</a></h2>
                `);
            });
            
            
        }
      });
}


function emitConsoleLog(message) {
    io.emit('consoleLog', message); // Emit the console log message to all connected clients
}


// terminal server
server.listen(4001, () => {
    console.log(`=======================================`);
    console.log(`Terminal Server started on http://localhost:4001/`);
    console.log(`=======================================\n`);
});
  // Listen for the 'syncToAtlas' event from the client and trigger the syncToAtlas function
io.on('connection', (socket) => {
    socket.on('syncToAtlas', () => {
      
      syncToAtlas();
      console.log("initialization started")
    });
});

//##############################################

app.get('/', (req, res) => {
  console.log('connecting to syncer...')

  const isReachable = require('is-reachable');

  async function checkInternetConnection() {
      try {
          const checks = [
              isReachable('google.com'),
              isReachable('facebook.com'),
              isReachable('twitter.com'),
              isReachable('youtube.com'),
              isReachable('github.com'),
              isReachable('comtechgingoog.com'),
          ];

          const results = await Promise.all(checks);
          console.log("INTERNET CHECKS " + results)
          const internetStatus = results.some(status => status === true);
      
          return internetStatus ? true : false;
      } catch (error) {
          return 'An error occurred while checking the internet connection';
      }
    
    }

    setTimeout(()=>{
      checkInternetConnection()
      .then(result => {
          console.log(result);
          if(result === false){
              res.redirect('http://192.168.1.205:4000/job-orders');
          } else {
              res.redirect('/initialize')
          }
      })
      .catch(error => console.error(error));
    }, 5000)


})

app.get('/initialize', (req, res) => {
    res.sendFile(__dirname + '/terminal.html');
});

